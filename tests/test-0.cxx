#include <cassert>

#include "parallel/parallel.hxx"

int main(int, char **) {
    using namespace parallel;
    std::future<int> a;
    {
        thread_pool tp { 32 };
        a = tp.execute([] () { return 1; });
        tp.await();
    }
    assert(a.get() == 1);
    return 0;
}
