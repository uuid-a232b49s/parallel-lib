#include <cassert>
#include <functional>

#include "parallel/parallel.hxx"

int main(int, char **) {
    using namespace parallel;
    std::vector<std::future<int>> futures;
    {
        thread_pool tp { 32 };
        std::vector<std::function<int(void)>> x = {
            [] () { return 1; },
            [] () { return 2; },
            [] () { return 3; },
            [] () { return 4; }
        };
        tp.executeBatch(std::move(x), futures);
        tp.await();
    }
    assert(futures[0].get() == 1);
    assert(futures[1].get() == 2);
    assert(futures[2].get() == 3);
    assert(futures[3].get() == 4);
    return 0;
}
