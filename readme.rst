parallel
========

What is this?
-------------

- a nano-sized (1 header, < 150 lines) thread pooling library


License
-------

Boost Software License - Version 1.0


CMake
-----

- interface library target: ``parallel-lib``

- example

  .. code-block :: cmake

    add_executable(my-executable ...)
    target_link_libraries(my-executable parallel-lib)

- see tests for more examples


Tested compilers
----------------

- gnu 10.2.1

- msvc 19
