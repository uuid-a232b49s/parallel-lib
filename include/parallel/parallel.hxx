#include <mutex>
#include <atomic>
#include <future>
#include <memory>
#include <thread>
#include <vector>
#include <utility>
#include <condition_variable>

namespace parallel {

    struct thread_pool {
    private:

        struct queuedTask {
            void(*exeTask)(void *);
            std::unique_ptr<void, void(*)(void *)> task;
            queuedTask(decltype(exeTask) exeTask, decltype(task) task) : exeTask(std::move(exeTask)), task(std::move(task)) {}
        };

        std::vector<queuedTask> m_tasks;
        std::vector<std::thread> m_threads;
        std::mutex m_taskMtx;
        std::condition_variable m_taskCdn;
        bool m_exit = false;

        void executor0() {
            while (true) {
                queuedTask task { nullptr, { nullptr, nullptr } };
                {
                    bool hasMore = false;
                    {
                        std::unique_lock<std::mutex> l { m_taskMtx };
                        while (true) {
                            if (m_exit) { return; }
                            if (!m_tasks.empty()) {
                                task = std::move(m_tasks.back());
                                m_tasks.pop_back();
                                hasMore = !m_tasks.empty();
                                break;
                            }
                            m_taskCdn.wait(l);
                        }
                    }
                    if (hasMore) { m_taskCdn.notify_one(); }
                }
                task.exeTask(task.task.get());
            }
        }

    public:

        thread_pool(int threads = 0) {
            if (threads <= 0) { threads = std::thread::hardware_concurrency(); }
            m_threads.resize(threads);
            for (auto & t : m_threads) {
                std::promise<void> p;
                auto f = p.get_future();
                t = std::thread([&] (decltype(p) * p) {
                    p->set_value();
                    while (true) {
                        try {
                            executor0();
                            break;
                        } catch (...) {}
                    }
                    }, &p);
                f.get();
            }
        }

        thread_pool(const thread_pool &) = delete;
        thread_pool(thread_pool &&) = delete;

        ~thread_pool() {
            {
                std::lock_guard<std::mutex> l { m_taskMtx };
                m_exit = true;
            }
            m_taskCdn.notify_all();
            for (auto & t : m_threads) {
                try {
                    t.join();
                } catch (std::system_error &) {}
            }
        }

        template<typename lambda_t>
        auto execute(lambda_t lambda) -> std::future<decltype(lambda())> {
            using pt = std::packaged_task<decltype(lambda())(void)>;
            std::future<decltype(lambda())> f;
            pt * p;
            decltype(queuedTask::task) qt { static_cast<void *>(p = new pt(std::move(lambda))), [] (void * p) { delete static_cast<pt *>(p); } };
            f = p->get_future();
            {
                std::lock_guard<std::mutex> l { m_taskMtx };
                m_tasks.emplace_back([] (void * t) { (*static_cast<pt *>(t))(); }, std::move(qt));
            }
            m_taskCdn.notify_one();
            return f;
        }

        template<typename container_t, typename futureContainer_t>
        void executeBatch(container_t container, futureContainer_t & futures) {
            {
                std::lock_guard<std::mutex> l { m_taskMtx };
                for (auto lambda : container) {
                    using pt = std::packaged_task<decltype(lambda())(void)>;
                    std::future<decltype(lambda())> f;
                    pt * p;
                    decltype(queuedTask::task) qt { static_cast<void *>(p = new pt(std::move(lambda))), [] (void * p) { delete static_cast<pt *>(p); } };
                    f = p->get_future();
                    {
                        m_tasks.emplace_back([] (void * t) { (*static_cast<pt *>(t))(); }, std::move(qt));
                    }
                    futures.emplace_back(std::move(f));
                }
            }
            m_taskCdn.notify_all();
        }

        void await() {
            std::unique_lock<std::mutex> l { m_taskMtx };
            while (!m_tasks.empty()) {
                m_taskCdn.wait_for(l, std::chrono::nanoseconds(10));
            }
        }

    };

}
